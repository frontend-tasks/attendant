import { useState } from 'react';
import { NavLink, useLocation } from "react-router-dom";
import { FaUser, FaLock, FaFacebook, FaTwitter, FaGoogle, FaLinkedin } from 'react-icons/fa';
import "./SignInForm.css"

function SignInForm() {
    const { pathname } = useLocation();
    const [username, setUsername] = useState('');
    const [pin, setPin] = useState('');

    const handleSignIn = (event) => {
        setUsername(event.target.value);
        setPin(event.target.value);
        event.preventDefault();
    };

    return (
        <div className="userLoginPage">
            <h2>
                {pathname === '/user' ? "Sign in as User" : "Sign in as Admin"}
            </h2>
            <form onSubmit={handleSignIn}>
                <div className="username">
                    <span className="icon"><FaUser /></span>
                    <input type="text" placeholder="Username" value={username} />
                </div>
                <div className="pincode">
                    <span className="icon"><FaLock /></span>
                    <input type="password" placeholder="Pincode" value={pin} maxLength="4" />
                </div>
                <button className="loginBtn" type="submit">LOGIN</button>
            </form>
            <p>Or sign in using social platforms</p>
            <div className="socialIcons">
                <FaFacebook />
                <FaTwitter />
                <FaGoogle />
                <FaLinkedin />
            </div>
            <div>
                {pathname === '/user' ? (
                    <p>
                        Or Are you an admin?
                        <NavLink to="/admin"> Sign in as admin instead?</NavLink>
                    </p>
                ) : (
                    <p>
                        Or Are you an user?
                        <NavLink to="/user"> Sign in as user instead?</NavLink>
                    </p>
                )}
            </div>
        </div>
    );
}

export default SignInForm;